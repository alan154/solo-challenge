# SOLO Code challenge: Frontend
## Instructions
For the Frontend section of the challenge you should use one of the following javascript frameworks:
* Vue.js
* React.js
* Angular.js

Use the API calls of the backend previuosly created.
You can use any other third party library of your choice if needed. 

### Goals
* List all Todos on screen, when te page loads.
* Creation of new Todos.
* Update of existing Todos.
* Deletion of Todos.

### Strech Goals (Not necessary for the evaluation, but will give you bonus points)
* Global State Management (Redux, Vuex, etc...)
* Styled UI
* Unit Testing
