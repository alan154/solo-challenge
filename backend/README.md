# SOLO Code challenge: Backend
## Instructions
For the Backend section of the challenge you should use:
* Nodejs
* Expressjs

In this folder are two files with data that could be useful to build the backend, this files can be used instead of the database.

The APIs created should be called by the Frontend.
You can use any other third party library of your choice if needed. 

### Goals
* Controller and route for the creation of new Todo.
* Controller and route for reading all Todos.
* Controller and route for updating an existing Todo.
* Controller and route for deleting and existing Todo.

### Strech Goals (Not necessary for the evaluation, but will give you bonus points)
* Error Handling (Middleware)
* Use typescript
* Unit Testing
