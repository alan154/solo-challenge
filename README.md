# Coding challenge
This page consists of the coding challenge for Full Stack JavaScript role at SOLO Trucking.

# Purpose
Aim of this test is three fold,

- evaluate your coding abilities 
- judge your technical experince
- understand how you design a solution

# How you will be judged
You will be scored on,

- coding standard, comments and style
- overall solution design
- appropriate use of source control
- clean code architecture

# Intructions

- Please use JavaScript, NodeJS, ExpressJS, and a Frontend Framework (Vue, React, Angular) for the following challenge
- Candidate should put their test results on a public code repository hosted on Gitlab or Github
- Once test is completed please share the repository URL to the team so they can review your work

# Challenge - Todo Manager

Write a web application that the Backend can manipulate Todos in a source file and can be access through an API and 
for the Frontend it needs to display the list of Todos, create new ones, update them and delete them. 

## Details

- In each folder you will see more detailed instructions and goals for the Frontend and Backend sections.
- Write a web application - backend built using JavaScript/NodeJS/ExpressJS and frontend developed using a Frontend Framework (Vue, React, Angular). 
- On page load the applications should load all the Todos in either a list or grid view.  
- Please make a README.md file with the instructions to run the project.
- The user should be able to create, update and delete the Todos.
- Use any other third party library of your choice if needed.
